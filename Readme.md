####Requirements to run :
```
docker
docker-compose
```
Tested on Ubuntu 20
####Download dependencies:

```
git clone git@gitlab.com:adrian.huminiuc/amb-admin.git
```

```
cd amb-admin
```
```
docker-compose up -d
```

####Add database schema + fixtures
```
docker exec -it amb_api npm run typeorm schema:drop 
docker exec -it amb_api npm run typeorm schema:sync 
docker exec -it amb_api node fixtures/fixtures.js 
```

###Open project link
[localhost:8080](http://localhost:8080)

TEST
