import Api from '../api/api';
import ApplicantApi from '../api/applicant-api';

class ApplyHandler {
    handle(slug, data): Promise<object> {
        return (new ApplicantApi(new Api('http://localhost:3000'))).applyToDiscount(slug, data)
        .catch((res) => {
            if (res.errorCode) {
                return Promise.reject({key: 'email', code: 'duplicate'});
            }
        });
    }
}

export default new ApplyHandler();