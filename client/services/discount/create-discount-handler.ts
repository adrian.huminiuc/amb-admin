import DiscountsApi from '../api/discounts-api';
import Api from '../api/api';

class CreateDiscountHandler {
    handle(data): Promise<object> {
        return (new DiscountsApi(new Api('http://localhost:3000'))).createDiscount(data)
        .catch((res) => {
            if (res.errorCode) {
                return Promise.reject({key: 'name', code: 'duplicate'});
            }
        });
    }
}

export default new CreateDiscountHandler();