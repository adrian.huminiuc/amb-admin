import Api from './api';

export default class ApplicantApi {
    constructor(readonly api: Api = null) {
        this.api = api || new Api();
    }

    async applyToDiscount(discountSlug: string, formData: object): Promise<any> {
        return this.api.postJson('/apply/' + discountSlug, formData);
    }
}