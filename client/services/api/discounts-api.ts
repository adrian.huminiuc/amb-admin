import Api from './api';

export default class DiscountsApi {
    constructor(readonly api: Api = null) {
        this.api = api || new Api();
    }

    async getAllDiscounts(): Promise<any> {
        return this.api.getJson('/discounts');
    }

    async getDiscountDetails(id: string): Promise<any> {
        return this.api.getJson('/discounts/' + id);
    }
    
    async getDiscountPublicDetails(slug: string): Promise<any> {
        return this.api.getJson('/discounts/public/' + slug);
    }

    async createDiscount(formData: object): Promise<any> {
        return this.api.postJson('/discounts', formData);
    }
}