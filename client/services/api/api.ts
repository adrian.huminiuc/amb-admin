export default class Api {
    headers = {'Content-Type': 'application/json'};

    constructor(readonly baseUrl = 'http://api:3000') {
        this.baseUrl = baseUrl;
    }

    async getJson(route: string): Promise<any> {
        const response = await fetch(this.baseUrl + route, {
            method: 'GET',
            headers: this.headers,
        });

        return response.json();
    }

    async postJson(route: string, data: object) {
        const response = await fetch(this.baseUrl + route, {
            method: 'POST',
            headers: this.headers,
            body: JSON.stringify(data)
        });

        if (response.status >= 400) {
            return Promise.reject(await response.json());
        }

        return response.json();
    }
}