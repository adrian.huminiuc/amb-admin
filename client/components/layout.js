import Sidebar from './sidebar';
import Topbar from './topbar';

export default function Layout({content}) {
    return (
        <div>
            <Sidebar />
            <Topbar />
            <main>
                {content}
            </main>
        </div>
    )
}