import Link from 'next/link';
import styles from '../styles/components/Sidebar.module.css';

export default function Sidebar() {
    return (
        <div className={styles.sidenav + ' shadow-xs'}>
            <Link href="/">
                <a>Discounts</a>
            </Link>
        </div>
    );
}