import styles from '../styles/components/Topbar.module.css'

export default function Topbar() {
    return (
    <nav className={styles.topbar + ' pin-t'}>
        <div className="container mx-auto flex flex-wrap items-center">
            <div className="flex w-full md:w-1/2 text-white font-extrabold">
                <a className="text-active no-underline hover:no-underline" href="/">
                    <span className="text-2xl pl-2"> Admin</span>
                </a>
            </div>
        </div>
    </nav>
    )
}