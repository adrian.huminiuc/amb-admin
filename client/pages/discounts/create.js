import Layout from '../../components/layout';
import styles from '../../styles/components/Create.module.css';
import {useForm} from 'react-hook-form';
import Router from 'next/router';
import CreateDiscountHandler from '../../services/discount/create-discount-handler';

export default function Create() {
    const {register, handleSubmit, setError, errors} = useForm();
    const onSubmit = data => CreateDiscountHandler.handle(data)
    .then(() => Router.push('/'))
    .catch((validationError) => {
        if (validationError.code === 'duplicate') {
            setError(validationError.key, {
                type: 'manual',
                message: 'Must be unique!',
            });
        }
    });

    return (
        <Layout content={
            <section className={styles.container + ' max-w-lg'}>
                <h1>Create new discount group</h1>
                <div className="w-full max-w-md">
                    <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmit)}>
                        <div className="mb-4">
                            <label>Discount course name</label>
                            <input
                                className="create-input focus:outline-none focus:shadow-outline"
                                name="name"
                                ref={register({required: {value: true, message: 'This field is required!'}})}/>
                            {errors.name && <span className="text-red-600"> {errors.name.message} </span>}
                        </div>

                        <div className="mb-4">
                            <label>Start date</label>
                            <input
                                className="create-input leading-tight focus:outline-none focus:shadow-outline"
                                name="start"
                                type="date"
                                ref={register}/>
                        </div>

                        <div className="mb-4">
                            <label>End date</label>
                            <input
                                className="create-input leading-tight focus:outline-none focus:shadow-outline"
                                name="end"
                                type="date"
                                ref={register}/>
                        </div>

                        <div className="mb-4">
                            <label>Discount rule</label>
                            <select
                                className="create-input focus:outline-none focus:shadow-outline"
                                name="rule" ref={register}>
                                <option value="50-50">Max 50%</option>
                                <option value="flat-11">Flat 11</option>
                            </select>
                        </div>

                        <div className="mb-4">
                            <input className="btn-success cursor-pointer" type="submit"/>
                        </div>
                    </form>
                </div>
            </section>
        }/>
    );
}