import DiscountsApi from '../../services/api/discounts-api';
import {useForm} from 'react-hook-form';
import Image from 'next/image'
import ApplyHandler from '../../services/applicant/apply-handler';
import { useState } from 'react';

export default function DiscountLandingPage({details}) {
    const {register, handleSubmit, errors} = useForm();
    const [applied, setApplied] = useState(false);

    const onSubmit = data => ApplyHandler.handle(details.slug, data)
    .then(()=> setApplied(true))
    
    return (
        <div className="flex flex-row ">
            <Image
                src="/university.jpg"
                alt="University pic"
                width={500}
                height={500}
            />
            <div className="ml-3 max-w-md p-5 theme-background shadow-xs">
                <p>
                    <span className="font-semibold">{details.name}</span>
                </p>
                <p>New discount opportunity</p>
                <p>Max discount: <span className="font-semibold">{details.maxDiscount}%</span></p>
                <p>Opens: {details.start}</p>
                <p>
                    Convince {details.applicantsNextLevel} more friends to unlock next level of discounts!
                </p>
                {applied ? 
                    <div className="shadow-md rounded px-8 pt-6 pb-8 mb-4">
                        <p>You are signed up for  this discount group!</p>
                    </div>
                 : <form className="shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit(onSubmit)}>
                     <div className="mb-4">
                         <label>Email</label>
                         <input
                             className="create-input focus:outline-none focus:shadow-outline"
                             name="email"
                             type="email"
                             ref={register({required: {value: true, message: 'This field is required!'}})}/>
                         {errors.email && <span className="text-red-600"> {errors.email.message} </span>}
                     </div>
                     <div className="mb-4">
                         <label>First name and last name</label>
                         <input
                             className="create-input focus:outline-none focus:shadow-outline"
                             name="fullname"
                             ref={register({required: {value: true, message: 'This field is required!'}})}/>
                         {errors.fullname && <span className="text-red-600"> {errors.fullname.message} </span>}
                     </div>
                     <div className="mb-4">
                         <input className="btn-success cursor-pointer" type="submit"/>
                     </div>
                 </form>
                }
            </div>
        </div>
    );
}

export async function getServerSideProps({query}) {
    return {
        props: {details: {...await (new DiscountsApi()).getDiscountPublicDetails(query.slug[0])}},
    };
}