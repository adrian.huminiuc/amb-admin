import Layout from '../../components/layout';
import DiscountsApi from '../../services/api/discounts-api';

export default function Details({details}) {
    return (
        <Layout content={
            <section className="flex flex-col items-center max-w-lg">
                <h1> Target details</h1>
                <div className="shadow-xs p-10 mb-3">
                    <p><span className="font-semibold">Discount group name:</span> {details.discount.name}</p>
                    <p><span className="font-semibold">Discount Rule: </span>{details.discount.rule}</p>
                    <p><span className="font-semibold">Total applicants:  </span>{details.discount.applicants}</p>
                    <p><span className="font-semibold">End date:  </span>{details.discount.end}</p>
                    <p><span className="font-semibold">CODE:  </span>{details.discount.code}</p>
                    <p><span className="font-semibold">Landing Page:  </span>{details.discount.slug}</p>
                    <div>
                        <button className="btn-success mr-1" title="Not implemented">Send Reminders</button>
                        <button className="btn-success" title="Not implemented">Close</button>
                    </div>
                </div>
                
                <div className=" pb-2">
                    <table className={'max-w-md table-fixed border shadow-xs'}>
                        <thead>
                        <tr className={'bg-gray-100'}>
                            <th className={'border px-4 py-2'}>Applicant Email</th>
                            <th className={'border px-4 py-2'}>Applied at</th>
                            <th>Reminders received</th>
                            <th>Reminders viewed</th>
                        </tr>

                        </thead>
                        <tbody>
                            {details.applicants.map(applicant => {
                                return <tr key={applicant.email}>
                                    <td className={'border px-4 py-1'}>{applicant.email}</td>
                                    <td className={'border px-4 py-1'}>{applicant.appliedDate}</td>
                                    <td className={'border px-4 py-1'}>0</td>
                                    <td className={'border px-4 py-1'}>0</td>
                                </tr>
                            })}
                        </tbody>
                    </table>
                </div>
            </section>
        }/>
    );
}

export async function getServerSideProps({query}) {
    return {
        props: { details: {...await (new DiscountsApi()).getDiscountDetails(query.id)} },
    }
}