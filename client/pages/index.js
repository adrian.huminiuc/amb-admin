import Head from 'next/head';
import Layout from '../components/layout';
import Link from 'next/link';
import DiscountsApi from '../services/api/discounts-api';

const DiscountsBody = ({discounts}) => {
    return (
        <div>
            <Head>
                <title>Manage discounts</title>
            </Head>
            <div className={'main bg-grey-lightest pt-2 px-6 flex flex-col'}>
                <Link href="/discounts/create">
                    <a className={'btn-success mb-1 self-end'}>Create Discount</a>
                </Link>

                <table className={'table-auto border shadow-xs'}>
                    <thead>
                    <tr className={'bg-gray-100'}>
                        <th className={'border px-4 py-2'}>Name</th>
                        <th className={'border px-4 py-2'}>Start</th>
                        <th className={'border px-4 py-2'}>End</th>
                        <th className={'border px-4 py-2'}>Rule</th>
                        <th className={'border px-4 py-2'}>Applicants</th>
                        <th className={'border px-4 py-2'}>Current Discount</th>
                        <th className={'border px-4 py-2'}># Next level</th>
                        <td className={'border px-4 py-2'}>Actions</td>
                    </tr>

                    </thead>
                    <tbody>
                        {discounts.map(discount => {
                            return <tr key={discount.id}>
                                <td className={'border px-4 py-2'}>{discount.name}</td>
                                <td className={'border px-4 py-2'}>{discount.start}</td>
                                <td className={'border px-4 py-2'}>{discount.end}</td>
                                <td className={'border px-4 py-2'}>{discount.rule}</td>
                                <td className={'border px-4 py-2'}>{discount.applicants}</td>
                                <td className={'border px-4 py-2'}>{discount.currentDiscountValue}%</td>
                                <td className={'border px-4 py-2'}>{discount.applicantsDiffNextLevel} more needed</td>
                                <td>
                                    <Link href={{pathname: '/discounts/details', query: {id: discount.id}}}>
                                        <a className={'btn-success mr-1'}>Details</a>
                                    </Link>
                                    <Link href={{pathname: '/discounts/' + discount.slug}}>
                                        <a className={'btn-success mr-1'}>Landing page</a>
                                    </Link>
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default function Discounts({discounts}) {
    return (
        <Layout content={<DiscountsBody discounts={discounts}/>}/>
    );
}

export async function getServerSideProps() {
    return {
        props: { discounts: [...await (new DiscountsApi()).getAllDiscounts()] }, 
    }
}
