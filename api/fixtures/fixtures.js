'use strict';

require('dotenv/config');
require('reflect-metadata');
require('ts-node/register');
const fixturesLoader = require('./app-fixtures.ts');

fixturesLoader.load().then(
    () => {
        console.log('Loaded fixtures!');
        process.exit(0);
    },
);
