import {University} from '../src/entity/university';
import {Course} from '../src/entity/course';
import {createConnection, getManager} from 'typeorm';
import {EntityManager} from 'typeorm/entity-manager/EntityManager';
import {Discount} from '../src/entity/discount';
import {Applicant} from '../src/entity/applicant';
import {slugify} from '../src/utils/url/slug';

const loadUniversities = async (manager: EntityManager) => {
    const universities = [
        'Augusta',
        'Liberta',
        'Costanta',
        'Maxima',
    ];

    await Promise.all(universities.map(async (item) => {
        const uni = new University();
        uni.name = item;
        await manager.save(uni);
    }));
};

const loadCourses = async (manager: EntityManager) => {
    const courses = [
        ['Augusta @ Winter 2020', '2020-10-01', '2020-12-30'],
        ['Augusta @ Winter 2021', '2021-01-01', '2021-03-30'],
        ['Augusta @ Spring 2021', '2021-04-01', '2021-06-30'],
        ['Augusta @ Summer 2021', '2021-07-01', '2021-10-30'],
    ];

    await Promise.all(courses.map(async (item) => {
        const [name, start, end] = item;
        const course = new Course();
        course.name = name;
        course.startDate = start;
        course.endDate = end;
        course.university = await manager.findOne(University, {name: 'Augusta'});

        await manager.save(course);
    }));
};


const loadDiscounts = async (manager: EntityManager) => {
    const discounts = [
        ['Augusta winter 2020 - 50 at 50%', '50-50', 'Augusta @ Winter 2020', '2020-09-01', '2020-09-30'],
    ];

    await Promise.all(discounts.map(async (item) => {
        const [name, rule, courseName, start, end] = item;
        const discount = new Discount();
        discount.name = name;
        discount.slug =  slugify(name);
        discount.rule = rule;
        discount.startDate = start;
        discount.endDate = end;
        discount.course = await manager.findOneOrFail(Course, {name: courseName});

        await manager.save(discount);
    }));
};


const loadApplicants = async (manager: EntityManager) => {
    const applicants = [];
    for(let i = 1; i < 25; i++) {
        applicants.push(
            [`test${i}@test.de`, `Applicant ${i}`]
        );
    }

    await Promise.all(applicants.map(async (item) => {
        const [email, name] = item;
        const applicant = new Applicant();
        applicant.email = email;
        applicant.name = name;
        applicant.applyDate = (new Date()).toISOString().split('T')[0];
        applicant.discount = await manager.findOne(Discount, {id: 1})

        await manager.save(applicant);
    }));
};

export async function load() {
    await createConnection();
    const manager = getManager();

    await loadUniversities(manager);
    await loadCourses(manager);
    await loadDiscounts(manager);
    await loadApplicants(manager);
}
