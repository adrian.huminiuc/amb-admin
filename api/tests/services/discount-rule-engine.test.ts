import { expect } from 'chai';
import DiscountRuleEngine, {RULES} from '../../src/services/discount-rule-engine'

describe('Discount Engine', () => {
    it('It should fetch the right rule discount value', () => {
        expect(
            DiscountRuleEngine.getDiscountValue(RULES.FIFTY_FIFTY, 0),
        ).to.eq(0);

        expect(
            DiscountRuleEngine.getDiscountValue(RULES.FIFTY_FIFTY, 20),
        ).to.eq(20);

        expect(
            DiscountRuleEngine.getDiscountValue(RULES.FIFTY_FIFTY, 37),
        ).to.eq(30);
    });
});