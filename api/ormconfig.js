const { DB_PORT, DB_USER, DB_PASSWORD, DB_DATABASE, DATABASE_HOST } = process.env;

module.exports = [{
    name: 'default',
    type: 'mysql',
    host: DATABASE_HOST,
    port: DB_PORT,
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    synchronize: true,
    entities: [
        "src/entity/*.ts"
    ],
    migrations: [
        "src/migrations/*.ts"
    ],
    cli: {
        entitiesDir: "src/entity",
        migrationsDir: "src/migrations",
    }
}];