export class DiscountListItem {
    constructor(
        readonly id: number,
        readonly name: string,
        readonly slug: string,
        readonly rule: string,
        readonly start: string,
        readonly end: string,
        readonly applicants: number,
        readonly currentDiscountValue: number,
        readonly applicantsDiffNextLevel: number
    ) {
    }
}