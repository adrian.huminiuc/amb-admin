export class DiscountPublicItem {
    constructor(
        readonly name: string,
        readonly slug: string,
        readonly start: string,
        readonly end: string,
        readonly maxDiscount: number,
        readonly applicantsNextLevel: number,
    ) {
    }
}