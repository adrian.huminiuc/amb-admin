import {DiscountListItem} from './discount-list-Item';

export class DiscountDetailsApplicantItem {
    constructor(readonly email: string, readonly appliedDate: string) {}
}

export class DiscountDetailsItem {
    constructor(
        readonly discount: DiscountListItem,
        readonly applicants: Array<DiscountDetailsApplicantItem>,
    ) {}
}