export const slugify = (text: string) => {
    return text.toLowerCase()
        .replace(/ +|_/g,'-')
        .replace(/[^\w-]+/g, '')

    
};