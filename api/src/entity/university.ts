import {Entity, Column, PrimaryColumn, OneToMany} from 'typeorm';
import {Course} from './course';

@Entity()
export class University {
    @PrimaryColumn({generated: 'increment'})
    id: number;

    @Column({unique: true})
    name: string;

    @OneToMany(() => Course, course => course.university)
    courses: Promise<Array<Course>>;
}