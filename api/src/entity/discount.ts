import {Entity, Column, PrimaryColumn, OneToOne, OneToMany, JoinColumn} from 'typeorm';
import {IsEnum} from 'class-validator';
import {Course} from './course';
import {Applicant} from './applicant';
import {RULES} from '../services/discount-rule-engine';

/**
 * Named discount only for consistency.(with specifications)
 * This is a line item, a product in an e-commerce sense.
 */
@Entity()
export class Discount {
    @PrimaryColumn({generated: 'increment'})
    id: number;

    @Column({unique: true})
    name: string;
    
    @Column({unique: true})
    slug: string;
    
    @Column()
    @IsEnum(RULES)
    rule: string;

    @OneToOne(() => Course)
    @JoinColumn()
    course: Course;

    @OneToMany(() => Applicant, applicants => applicants.discount, {
        cascade: true,
    })
    applicants: Promise<Array<Applicant>>;

    @Column()
    startDate: string;

    @Column()
    endDate: string;

    @Column()
    code: string = '';

    @Column()
    closed: boolean = false;
}