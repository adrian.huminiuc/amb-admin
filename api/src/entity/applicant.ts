import {Entity, Column, PrimaryColumn, OneToMany, ManyToOne} from 'typeorm';
import {Discount} from './discount';

@Entity()
export class Applicant {
    @PrimaryColumn({ generated: 'increment' })
    id: number;

    @Column()
    email: string;

    @Column()
    name: string;

    @ManyToOne(() => Discount, discount => discount.applicants)
    discount: Discount;
    
    @Column()
    applyDate: string;
}