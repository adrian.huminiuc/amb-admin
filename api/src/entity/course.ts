import {Entity, Column, PrimaryColumn, ManyToOne} from 'typeorm';
import {University} from './university';

@Entity()
export class Course {
    @PrimaryColumn({ generated: 'increment' })
    id: number;

    @Column({ unique: true })
    name: string;

    @Column()
    startDate: string;

    @Column()
    endDate: string;

    @ManyToOne(() => University, university => university.courses, {
        cascade: true,
    })
    university: University;
}