import {IExpressWithJson} from 'express-with-json';
import {getCustomRepository, getManager} from 'typeorm';
import * as express from 'express';
import {Discount} from '../entity/discount';
import DiscountListItemMapper from '../services/dto-mappers/discount-list-item-mapper';
import ApplicantsRepository from '../repositories/applicants-repository';
import {Applicant} from '../entity/applicant';
import {DiscountDetailsApplicantItem, DiscountDetailsItem} from '../dto/discount-details-item';

const manager = getManager();

export async function getDiscountDetails(req: express.Request) {
    const discount = await manager.findOne(Discount, {id: +req.params.id});
    const applicantsRepository: ApplicantsRepository = getCustomRepository(ApplicantsRepository);

    if (discount) {
        const applicants = await manager.find(Applicant, {'discount': discount});

        return new DiscountDetailsItem(
            DiscountListItemMapper.getDto(discount, await applicantsRepository.getApplicantsCount(discount)),
            applicants.map(applicant => new DiscountDetailsApplicantItem(applicant.email, applicant.applyDate)),
        );
    }

    throw new Error('Cannot find discount!');
}

export default (app: IExpressWithJson) => {
    app.getJson('/discounts/:id', getDiscountDetails);
}