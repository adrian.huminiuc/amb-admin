import {IExpressWithJson} from 'express-with-json';
import {getCustomRepository, getManager} from 'typeorm';
import * as express from 'express';
import {Discount} from '../entity/discount';
import ApplicantsRepository from '../repositories/applicants-repository';
import {DiscountPublicItem} from '../dto/discount-public-item';
import DiscountRuleEngine from '../services/discount-rule-engine';

const manager = getManager();

export async function getDiscountPublicDetails(req: express.Request) {
    const discount = await manager.findOne(Discount, {slug: req.params.slug});
    const applicantsRepository: ApplicantsRepository = getCustomRepository(ApplicantsRepository);

    if (discount) {
        const applicants = await applicantsRepository.getApplicantsCount(discount);

        return new DiscountPublicItem(
            discount.name,
            discount.slug,
            discount.startDate,
            discount.endDate,
            DiscountRuleEngine.getMaxDiscount(discount.rule),
            DiscountRuleEngine.getNrOfApplicantsNeededForNextDiscount(discount.rule, applicants),
        );
    }

    throw new Error('Cannot find discount with specified slug');
}

export default (app: IExpressWithJson) => {
    app.getJson('/discounts/public/:slug', getDiscountPublicDetails);
}