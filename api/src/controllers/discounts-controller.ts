import {getManager} from 'typeorm';
import {validateOrReject} from 'class-validator';
import {IExpressWithJson} from 'express-with-json/dist';
import {Discount} from '../entity/discount';
import {DiscountListItem} from '../dto/discount-list-Item';
import {getCustomRepository} from 'typeorm';
import ApplicantsRepository from '../repositories/applicants-repository';
import DiscountListItemMapper from '../services/dto-mappers/discount-list-item-mapper';
import * as express from 'express';
import {slugify} from '../utils/url/slug';

const manager = getManager();

export async function getDiscounts(): Promise<DiscountListItem[]> {
    const discounts = await manager.find(Discount);
    const applicantsRepository: ApplicantsRepository = getCustomRepository(ApplicantsRepository);

    return await Promise.all(discounts.map(async (item: Discount) => {
        return DiscountListItemMapper.getDto(item, await applicantsRepository.getApplicantsCount(item));
    }));
}

export async function createDiscount(req: express.Request, res: express.Response): Promise<object> {
    const {name, rule, start, end} = req.body;

    const discount = new Discount();
    discount.name = name;
    discount.rule = rule;
    discount.startDate = start;
    discount.endDate = end;
    discount.slug = slugify(name);

    let entity;
    try {
        await validateOrReject(discount)
        entity = await manager.save(discount);
    } catch (error) {
        res.status(400);
        return {id: -1, errorCode: error?.code};
    }

    res.status(201);
    return {id: entity.id};
}

export default (app: IExpressWithJson) => {
    app.getJson('/discounts', getDiscounts);
    app.postJson('/discounts', createDiscount);
}