import {IExpressWithJson} from 'express-with-json';
import {getManager} from 'typeorm';
import * as express from 'express';
import {Discount} from '../entity/discount';
import {Applicant} from '../entity/applicant';

const manager = getManager();

export async function applyToDiscount(req: express.Request, res: express.Response) {
    const discount = await manager.findOne(Discount, {slug: req.params.slug});
    const {email, fullname} = req.body;

    if (discount) {
        const applicant = new Applicant();
        applicant.email = email;
        applicant.name = fullname;
        applicant.discount = discount;
        applicant.applyDate = (new Date()).toISOString().split('T')[0];

        try {
            await manager.save(applicant);
        } catch (error) {
            res.status(400);
            return {id: -1, errorCode: 'duplicate'};
        }

        res.status(201);
        return {id: applicant.id};
    }

    throw new Error('This discount group is not available!' + req.params.slug);
}

export default (app: IExpressWithJson) => {
    app.postJson('/apply/:slug', applyToDiscount);
}