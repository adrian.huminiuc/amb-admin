import {EntityManager, EntityRepository} from 'typeorm';
import {Discount} from '../entity/discount';
import {Applicant} from '../entity/applicant';

@EntityRepository()
export default class ApplicantsRepository {
    constructor(private manager: EntityManager) {}

    async getApplicantsCount(discount: Discount) {
        return (await this.manager.createQueryBuilder(Applicant, 'a')
        .select('count(*) as count')
        .where('a.discount = :discount', {discount: discount.id})
        .getRawOne())?.count || 0;
    }
}