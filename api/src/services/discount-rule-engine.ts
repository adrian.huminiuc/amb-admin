export enum RULES {
    FIFTY_FIFTY = '50-50',
    FLAT_11='flat-11'
}

class DiscountRuleApplicantStep {
    constructor(
        readonly min: number,
        readonly max: number,
        readonly value: number,
    ) {
    }
}

class DiscountRule {
    constructor(
        readonly name: string,
        readonly steps: Array<DiscountRuleApplicantStep>,
    ) {
    }
}

export class DiscountRuleEngine {
    public rules = [
        new DiscountRule(RULES.FIFTY_FIFTY, [
            new DiscountRuleApplicantStep(0, 9, 0),
            new DiscountRuleApplicantStep(10, 19, 10),
            new DiscountRuleApplicantStep(20, 29, 20),
            new DiscountRuleApplicantStep(30, 39, 30),
            new DiscountRuleApplicantStep(40, 49, 40),
            new DiscountRuleApplicantStep(50, Infinity, 50),
        ]),
        new DiscountRule(RULES.FLAT_11, [
            new DiscountRuleApplicantStep(0, Infinity, 11),
        ]),
    ];

    getDiscountValue(ruleName: string, applicants: number): number {
        return this.findDiscountStep(ruleName, applicants)?.value;
    }

    getMaxDiscount(ruleName: string): number {
        const rule = this.findDiscountRule(ruleName);
        return rule.steps[rule.steps.length - 1].value;
    }
    
    getNrOfApplicantsNeededForNextDiscount(ruleName: string, applicants: number): number {
        const rule = this.findDiscountRule(ruleName);
        const step = this.findDiscountStep(ruleName, applicants);
        const ruleIndex = rule.steps.indexOf(step);
        
        if (ruleIndex === -1) {
            throw Error('Cannot find rule with name' + ruleName);
        }

        if (!rule.steps[ruleIndex + 1]) {
            return 0;
        }

        return Math.max(0, rule.steps[ruleIndex + 1]?.min - applicants);
    }

    private findDiscountRule(ruleName: string): DiscountRule {
        return this.rules.find(rule => rule.name === ruleName);
    }

    private findDiscountStep(ruleName: string, applicants: number): DiscountRuleApplicantStep {
        return this.findDiscountRule(ruleName).steps.find(step => step.min <= applicants && step.max >= applicants);
    }
}

export default new DiscountRuleEngine();