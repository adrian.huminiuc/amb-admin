import {Discount} from '../../entity/discount';
import {DiscountListItem} from '../../dto/discount-list-Item';
import DiscountRuleEngine from '../discount-rule-engine';

class DiscountListItemMapper {
    getDto(discount: Discount, applicants: number): DiscountListItem {
        return new DiscountListItem(
            discount.id,
            discount.name,
            discount.slug,
            discount.rule,
            discount.startDate,
            discount.endDate,
            applicants,
            DiscountRuleEngine.getDiscountValue(discount.rule, applicants),
            DiscountRuleEngine.getNrOfApplicantsNeededForNextDiscount(discount.rule, applicants),
        );
    }
}

export default new DiscountListItemMapper();